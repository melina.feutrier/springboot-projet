package com.todo.todolist.services;

import com.todo.todolist.models.Task;
import com.todo.todolist.repositories.TaskRepository;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;


@SpringBootTest
class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskService taskService;

    @Test
    void create() {
        //Task newTask = new Task("taks 1", false, "Faire fonctionner les tests.");
        //taskRepository.save(newTask);
    }

    @Test
    void getTasks() {
        MockitoAnnotations.initMocks(this);

        //insertion des données test
        //Task task1 = new Task();       
        task1.setId(1);
        task1.setTask("Task 1");

        Task task2 = new Task();
        task2.setId(2);
        task2.setTask("Task 2");

        List<Task> taskList = new ArrayList<>();
        taskList.add(task1);
        taskList.add(task2);

        
        when(taskRepository.findAll()).thenReturn(taskList);    //Definition du comportament du mock

        List<Task> result = taskService.getTasks();     //méthode à tester 

        assertEquals(2, result.size()); //vérification du resultat

    }


    @Test
    void findTaskById() {
    }

    @Test
    void findAllCompletedTask() {
    }

    @Test
    void findAllUncompletedTask() {
    }

    @Test
    void delete() {
    }

    @Test
    void update() {
    }
}