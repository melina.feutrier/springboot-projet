package com.todo.todolist.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data                               // generation des methodes getter, setter, toString
@Table(name = "Task")               // nom de la table dans notre BDD

public class Task {
    @Id                             // clé primaire
    @GeneratedValue                 // generation automatique dans la bdd
    private Long id;
    private String task;
    private String description;
    private boolean complete;       // de type true/false

    // constructeur
    public Task() {
    }
    public Task(String task, boolean complete, String description) {    //constructeur qui accepté des parametres pour instanciée la clase et on etabli les valeurs du champs
        this.task = task;
        this.complete = complete;
        this.description = description;
    }

    //      Getter/ Setter pour accèder ou modifier les données
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTask() {
        return task;
    }
    public void setTask(String task) {
        this.task = task;
    }
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    public boolean isComplete() {
        return complete;
    }
    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
