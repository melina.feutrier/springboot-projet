package com.todo.todolist.services;

import com.todo.todolist.models.Task;
import com.todo.todolist.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {                  //Interface entre le contrôleur et le repository pour administration des tâches

    @Autowired
    private TaskRepository taskRepository;  //Accès au repository

    /*CODE POUR COMMENCER LE TEST

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }*/

    public Task create(Task task)           //Utilisation d'un exceptions en cas d'échouer au moment d'ajouter une tâche
    {
        try{
           return taskRepository.save(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to create");
        }
    }

    public List<Task> getTasks()            //Récuperation de toutes les tâches en forme de liste
    {
        return taskRepository.findAll();
    }

    public Task findTaskById(Long id)       //Touver une tâche par l'Id avec son éxception
    {
        Optional<Task> task = taskRepository.findById(id);

        if(task.isPresent())
        {
          return task.get();
        }else
        {
            throw new TaskNotFoundException("Task not found");
        }
    }

    public List<Task> findAllCompletedTask() //Touver toutes les taches completées avec son exception
    {
        try {
            return taskRepository.findTaskByCompleteTrue();
        }catch(Exception e)
        {
            throw new TaskNotFoundException("Tasks not found");
        }
    }

    public List<Task> findAllUncompletedTask()  //Touver toutes les taches non completées avec son exception
    {
        try {
            return taskRepository.findTaskByCompleteFalse();
        }catch(Exception e)
        {
            throw new TaskNotFoundException("Tasks not found");
        }
    }

    public void delete(Task task)               //Méthode pour suppression avec son exception en cas d'échec
    {
        try{
            taskRepository.delete(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to delete");
        }

    }

    public Task update(Task task)               //Méthode pour modifier avec son exception en cas d'échec
    {
        try{
            return taskRepository.save(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to update");
        }
    }



    public static class TaskNotFoundException extends RuntimeException {   //Exception lorsqu'une tâche n'est pas trouvée
        public TaskNotFoundException(String message) {
            super(message);
        }
    }

    public class TaskException extends RuntimeException {   //Exception liée aux opérations sur les tâches, comme l'échec de la création, la suppression ou la mise à jour
        public TaskException(String message) {
            super(message);
        }
    }


}
