package com.todo.todolist.repositories;

import com.todo.todolist.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//Repository fournit des méthodes pour effectuer des opération CRUD
@Repository                                         //Composant Spring traitée comme un bean
public interface TaskRepository extends JpaRepository<Task, Long> {
    public List<Task> findTaskByCompleteTrue();     //Requête qui récupère toutes les tâches marquées comme complètes
    public List<Task> findTaskByCompleteFalse();    //Récupère toute les tâches non complètes

    public List<Task> findAll();                    //Récupère toutes les tâches dans la base de données.
    public Task getById(Long Id);                   //Récupère une tâche par son ID
}
