package com.todo.todolist.controllers;


import com.todo.todolist.models.Task;
import com.todo.todolist.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//--------- CRUD -------------
@RestController
@RequestMapping("api/v1/")
public class TaskController {
    @Autowired                                                   //injection de dépendances, en permettant d'utiliser les fonctionnalités du service dans le méthode
    private TaskService taskService;

    @GetMapping("/tasks")                                     //méthode get pour obtenir toute la liste de taches avec url /task
    public ResponseEntity<List<Task>> getAll()
    {
        return ResponseEntity.ok(taskService.getTasks());       //Encapsulation dans l'objet ReponseEntity (http OK 200)
    }

    @GetMapping("/taskCompleted")                            //Récuperation des tâches completées, methode GET
    public ResponseEntity<List<Task>> getAllComplete()
    {
        return ResponseEntity.ok(taskService.findAllCompletedTask());
    }

    @GetMapping("/taskUncompleted")                         //Récuperation des tâches non completées, methode get
    public ResponseEntity<List<Task>> getAllUncomplete()
    {
        return ResponseEntity.ok(taskService.findAllUncompletedTask());
    }

    @PostMapping("/addtask")                                //Ajout d'une nouvelle tâche, methode POST
    public ResponseEntity<Task> create(@RequestBody Task task)//Deserialization du request http
    {
        return ResponseEntity.ok(taskService.create(task));
    }

    @PutMapping("/{id}")                                    //Méthode update pour modification d'une täche depuis l'ID
    public ResponseEntity<Task> update(@PathVariable Long id, @RequestBody Task task)
    {
        task.setId(id);
        return ResponseEntity.ok(taskService.update(task));
    }

    @DeleteMapping("/{id}")                                  //Méthode delete pour suppression d'une täche depuis l'ID
    public ResponseEntity<Boolean> delete(@PathVariable Long id)
    {
        Task taskToDelete = taskService.findTaskById(id);
        taskService.delete(taskToDelete);
        return ResponseEntity.ok(true);
    }

}
