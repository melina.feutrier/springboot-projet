# Springboot project

## Gestionnaire de Tâches To-Do

Ce projet consiste en une application To-Do simple permettant aux utilisateurs de gérer leur liste de tâches. L'application offre des fonctionnalités de base telles que l'ajout de tâches, le marquage des tâches comme complètes et la suppression de tâches.

## Fonctionnalités attendues

- Gestion des Tâches
    Ajouter une nouvelle tâche avec un titre et une description.
    Marquer une tâche comme complète.
    Supprimer une tâche de la liste.
- Accès aux Données
    Utiliser Spring Data JPA pour la couche d'accès aux données.
    Configurer une base de données légère (par exemple, H2) pour stocker les informations sur les tâches.
- API REST
    Implémenter une API REST pour les opérations de gestion des tâches (CRUD).
    Utiliser les opérations HTTP appropriées (GET, POST, PUT, DELETE) pour chaque fonctionnalité.
    Retourner les réponses au format JSON.

# Installation 
- Clonez le projet : git clone https://gitlab.com/melina.feutrier/springboot-projet
- Accédez au répertoire du projet : cd todolist
- Exécutez et lancez l'application depuis votre IDE
- L'application sera accessible à l'adresse http://localhost:8080/api/v1/

# Endpoints
Utilisation d'url grâce à Postman
- GET http://localhost:8080/api/v1/tasks - liste toutes les tâches
- POST http://localhost:8080/api/v1/addtask - ajout d'un tâche
- PUT http://localhost:8080/api/v1/tasks/{id} - modification d'un tâche
- DELETE http://localhost:8080/api/v1/tasks/{id} -suppression d'un tâche 

# Accès au base de données 
- Url: jdbc:h2:file:./testdb
- Username: sa
- Password: password

